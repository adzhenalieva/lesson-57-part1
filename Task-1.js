const tasks = [

    {id: 234, title: 'Create user registration API', timeSpent: 4, category: 'Backend', type: 'task'},
    {id: 235, title: 'Create user registration UI', timeSpent: 8, category: 'Frontend', type: 'task'},
    {id: 237, title: 'User sign-in via Google UI', timeSpent: 3.5, category: 'Frontend', type: 'task'},
    {id: 238, title: 'User sign-in via Google API', timeSpent: 5, category: 'Backend', type: 'task'},
    {id: 241, title: 'Fix account linking', timeSpent: 5, category: 'Backend', type: 'bug'},
    {id: 250, title: 'Fix wrong time created on new record', timeSpent: 1, category: 'Backend', type: 'bug'},
    {id: 262, title: 'Fix sign-in failed messages', timeSpent: 2, category: 'Frontend', type: 'bug'},
];

let resultFrontEnd = tasks.reduce(function(sum, task) {
    if(task.category === "Frontend"){
        sum = sum + task.timeSpent
    } return sum
}, 0);
console.log("Time spent on 'FrontEnd' is " + resultFrontEnd);


let resultBug = tasks.reduce(function(sum, task) {
    if(task.type === "bug"){
        sum = sum + task.timeSpent
    } return sum
}, 0);

console.log("Time spent on 'bug' is " + resultBug);

let UI = tasks.filter(function(task) {
    return task.title.includes("UI");
});

console.log("There are " + UI.length + " tasks contained word 'UI'");

let resultTasks = tasks.reduce(function(sum, task) {
    const category = task.category;
    if(category in sum){
        sum[category]++;
    } else {
        sum[category] = 1;
    }

    return sum;

}, {});
console.log(resultTasks);

let resultTimeMoreThanFour = tasks.reduce(function(sum, task) {
    const category = task.timeSpent;
    let title = task.title;
    if(category >= 4){
        sum.push({title, category})
    }
    return sum;
}, []);
console.log(resultTimeMoreThanFour);